package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.UserDetails;

import java.math.BigInteger;

public interface UserDetailsDao extends GenericDao<UserDetails, Long> {
}
