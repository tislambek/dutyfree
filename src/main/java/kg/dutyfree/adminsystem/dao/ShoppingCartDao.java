package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.ShoppingCart;

import java.math.BigInteger;

public interface ShoppingCartDao extends GenericDao<ShoppingCart, Long> {
}
