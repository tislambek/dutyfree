package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.SalespointWarehouse;

import java.math.BigInteger;

public interface SalespointWarehouseDao extends GenericDao<SalespointWarehouse, Long> {
}
