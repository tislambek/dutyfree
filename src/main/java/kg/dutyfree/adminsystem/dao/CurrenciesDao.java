package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.Currencies;

import java.math.BigInteger;

public interface CurrenciesDao extends GenericDao<Currencies, Long> {
}
