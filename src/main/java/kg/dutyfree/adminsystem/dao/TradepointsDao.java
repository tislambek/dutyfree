package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.Tradepoints;

import java.math.BigInteger;

public interface TradepointsDao extends GenericDao<Tradepoints, Long> {
}
