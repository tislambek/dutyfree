package kg.dutyfree.adminsystem.dao;


import kg.dutyfree.adminsystem.entity.User;

import java.math.BigInteger;

/**
 * @author aziko
 */

public interface UserDao extends GenericDao<User, Long> {
}