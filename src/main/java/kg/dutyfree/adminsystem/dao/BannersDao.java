package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.entity.Banners;

import java.math.BigInteger;

public interface BannersDao extends GenericDao<Banners, Long> {
}
