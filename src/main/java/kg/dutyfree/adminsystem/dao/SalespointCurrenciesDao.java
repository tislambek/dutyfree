package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.SalespointCurrencies;

import java.math.BigInteger;

public interface SalespointCurrenciesDao extends GenericDao<SalespointCurrencies, Long> {
}
