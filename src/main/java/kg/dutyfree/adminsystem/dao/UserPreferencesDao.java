package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.UserPreferences;

import java.math.BigInteger;

public interface UserPreferencesDao extends GenericDao<UserPreferences, Long> {
}
