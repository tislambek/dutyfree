package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.SalespointDetails;

import java.math.BigInteger;

public interface SalespointDetailsDao extends GenericDao<SalespointDetails, Long> {
}
