package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.CurrenciesExchangeRates;

import java.math.BigInteger;

public interface CurrenciesExchangeRatesDao extends GenericDao<CurrenciesExchangeRates, Long> {
}
