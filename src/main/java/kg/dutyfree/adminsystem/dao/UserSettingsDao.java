package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.UserSettings;

import java.math.BigInteger;

public interface UserSettingsDao extends GenericDao<UserSettings, Long> {
}
