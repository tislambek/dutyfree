package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.entity.Country;

import java.math.BigInteger;

public interface CountryDao extends GenericDao<Country, Long> {
}
