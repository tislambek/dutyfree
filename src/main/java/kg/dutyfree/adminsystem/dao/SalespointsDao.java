package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.Salespoints;

import java.math.BigInteger;

public interface SalespointsDao extends GenericDao<Salespoints, Long> {
}
