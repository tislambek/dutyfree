package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.Orders;

import java.math.BigInteger;

public interface OrdersDao extends GenericDao<Orders, Long> {
}
