package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.entity.Role;

import java.math.BigInteger;

/**
 * Created by kbakytbekov on 07.09.2017.
 */
public interface RoleDao extends GenericDao<Role, Long> {
}
