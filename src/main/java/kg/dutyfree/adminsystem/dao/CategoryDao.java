package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.entity.Category;

import java.math.BigInteger;

public interface CategoryDao extends GenericDao<Category, Long> {
}
