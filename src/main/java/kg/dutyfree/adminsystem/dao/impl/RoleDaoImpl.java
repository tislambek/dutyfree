package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.RoleDao;
import kg.dutyfree.adminsystem.entity.Role;

import javax.persistence.EntityManager;
import java.math.BigInteger;

/**
 * Created by kbakytbekov on 07.09.2017.
 */
public class RoleDaoImpl extends GenericDaoImpl<Role, Long> implements RoleDao {
    public RoleDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
