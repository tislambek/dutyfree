package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.CurrenciesDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.Currencies;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class CurrenciesDaoImpl extends GenericDaoImpl<Currencies, Long> implements CurrenciesDao {
    public CurrenciesDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
