package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.PromoSalesDao;
import kg.dutyfree.adminsystem.entity.PromoSales;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class PromoSalesDaoImpl extends GenericDaoImpl<PromoSales, Long> implements PromoSalesDao {
    public PromoSalesDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
