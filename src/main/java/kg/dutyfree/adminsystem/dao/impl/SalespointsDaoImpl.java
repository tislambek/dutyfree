package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.SalespointsDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.Salespoints;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class SalespointsDaoImpl extends GenericDaoImpl<Salespoints, Long> implements SalespointsDao {
    public SalespointsDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
