package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.ProductsDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.Products;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class ProductsDaoImpl extends GenericDaoImpl<Products, Long> implements ProductsDao {
    public ProductsDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
