package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.UserPreferencesDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.UserPreferences;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class UserPreferencesDaoImpl extends GenericDaoImpl<UserPreferences, Long> implements UserPreferencesDao {
    public UserPreferencesDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
