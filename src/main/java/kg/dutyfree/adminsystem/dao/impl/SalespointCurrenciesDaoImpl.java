package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.SalespointCurrenciesDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.SalespointCurrencies;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class SalespointCurrenciesDaoImpl extends GenericDaoImpl<SalespointCurrencies, Long> implements SalespointCurrenciesDao {
    public SalespointCurrenciesDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
