package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.UserDao;
import kg.dutyfree.adminsystem.entity.User;

import javax.persistence.EntityManager;
import java.math.BigInteger;

/**
 * @author aziko
 */
public class UserDaoImpl extends GenericDaoImpl<User, Long> implements UserDao {

    public UserDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }


}
