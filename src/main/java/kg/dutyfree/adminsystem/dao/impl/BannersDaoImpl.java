package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.BannersDao;
import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.Banners;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class BannersDaoImpl extends GenericDaoImpl<Banners, Long> implements BannersDao {
    public BannersDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
