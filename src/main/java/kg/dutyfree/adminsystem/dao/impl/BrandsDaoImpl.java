package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.BrandsDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.Brands;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class BrandsDaoImpl extends GenericDaoImpl<Brands, Long> implements BrandsDao {
    public BrandsDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
