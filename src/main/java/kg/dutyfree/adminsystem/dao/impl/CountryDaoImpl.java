package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.CountryDao;
import kg.dutyfree.adminsystem.entity.Country;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class CountryDaoImpl extends GenericDaoImpl<Country, Long> implements CountryDao {
    public CountryDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
