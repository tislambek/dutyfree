package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.OrderDetailsDao;
import kg.dutyfree.adminsystem.dao.OrdersDao;
import kg.dutyfree.adminsystem.entity.OrderDetails;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class OrderDetailsDaoImpl extends GenericDaoImpl<OrderDetails, Long> implements OrderDetailsDao {
    public OrderDetailsDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
