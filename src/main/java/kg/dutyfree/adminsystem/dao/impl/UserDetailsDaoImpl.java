package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.UserDetailsDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.UserDetails;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class UserDetailsDaoImpl extends GenericDaoImpl<UserDetails, Long> implements UserDetailsDao {
    public UserDetailsDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
