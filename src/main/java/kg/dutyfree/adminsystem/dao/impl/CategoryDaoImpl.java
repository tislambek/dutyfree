package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.CategoryDao;
import kg.dutyfree.adminsystem.entity.Category;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class CategoryDaoImpl extends GenericDaoImpl<Category, Long> implements CategoryDao {
    public CategoryDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
