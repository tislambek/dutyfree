package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.OrdersDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.Orders;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class OrdersDaoImpl extends GenericDaoImpl<Orders, Long> implements OrdersDao {
    public OrdersDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
