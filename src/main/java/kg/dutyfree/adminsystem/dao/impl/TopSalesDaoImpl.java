package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.TopSalesDao;
import kg.dutyfree.adminsystem.entity.TopSales;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class TopSalesDaoImpl extends GenericDaoImpl<TopSales, Long> implements TopSalesDao {
    public TopSalesDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
