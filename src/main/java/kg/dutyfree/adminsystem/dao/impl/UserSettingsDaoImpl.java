package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.UserSettingsDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.UserSettings;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class UserSettingsDaoImpl extends GenericDaoImpl<UserSettings, Long> implements UserSettingsDao {
    public UserSettingsDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
