package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.TradepointsDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.Tradepoints;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class TradepointsDaoImpl extends GenericDaoImpl<Tradepoints, Long> implements TradepointsDao {
    public TradepointsDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
