package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.SalespointWarehouseDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.SalespointWarehouse;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class SalespointWarehouseDaoImpl extends GenericDaoImpl<SalespointWarehouse, Long> implements SalespointWarehouseDao {
    public SalespointWarehouseDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
