package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.CurrenciesExchangeRatesDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.CurrenciesExchangeRates;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class CurrenciesExchangeRatesDaoImpl extends GenericDaoImpl<CurrenciesExchangeRates, Long> implements CurrenciesExchangeRatesDao {
    public CurrenciesExchangeRatesDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
