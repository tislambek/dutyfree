package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.ShoppingCartDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.ShoppingCart;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class ShoppingCartDaoImpl extends GenericDaoImpl<ShoppingCart, Long> implements ShoppingCartDao {
    public ShoppingCartDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
