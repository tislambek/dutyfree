package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.SalespointDetailsDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.SalespointDetails;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class SalespointDetailsDaoImpl extends GenericDaoImpl<SalespointDetails, Long> implements SalespointDetailsDao {
    public SalespointDetailsDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
