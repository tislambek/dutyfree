package kg.dutyfree.adminsystem.dao.impl;

import kg.dutyfree.adminsystem.dao.SubcategoriesDao;
import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.Subcategories;

import javax.persistence.EntityManager;
import java.math.BigInteger;

public class SubcategoriesDaoImpl extends GenericDaoImpl<Subcategories, Long> implements SubcategoriesDao {
    public SubcategoriesDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }
}
