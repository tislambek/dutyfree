package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.entity.PromoSales;

import java.math.BigInteger;

public interface PromoSalesDao extends GenericDao<PromoSales, Long> {
}
