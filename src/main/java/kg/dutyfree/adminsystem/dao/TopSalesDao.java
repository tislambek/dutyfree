package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.entity.TopSales;

import java.math.BigInteger;

public interface TopSalesDao extends GenericDao<TopSales, Long> {
}
