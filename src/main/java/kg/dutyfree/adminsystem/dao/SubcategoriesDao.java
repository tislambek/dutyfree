package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.Subcategories;

import java.math.BigInteger;

public interface SubcategoriesDao extends GenericDao<Subcategories, Long> {
}
