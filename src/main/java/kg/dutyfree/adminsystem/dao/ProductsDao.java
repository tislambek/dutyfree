package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.entity.Products;

import java.math.BigInteger;

public interface ProductsDao extends GenericDao<Products, Long> {
}
