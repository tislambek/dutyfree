package kg.dutyfree.adminsystem.dao;

import kg.dutyfree.adminsystem.dao.impl.GenericDaoImpl;
import kg.dutyfree.adminsystem.entity.OrderDetails;

import java.math.BigInteger;

public interface OrderDetailsDao extends GenericDao<OrderDetails, Long> {
}
