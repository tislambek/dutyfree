package kg.dutyfree.adminsystem.ws;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;


@ApplicationPath("/api")
public class JaxRsActivator extends Application {
    /* class body intentionally left blank */
    @Override

    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }
    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */

    private void addRestResourceClasses(Set<Class<?>> resources) {
//        resources.add(EqueueRestController.class);
//        resources.add(UserRestController.class);
//        resources.add(CenterRestController.class);
//        resources.add(CategoryRestController.class);
//        resources.add(AndroidRestController.class);
    }

}
