package kg.dutyfree.adminsystem.listener;

import javax.ejb.EJB;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author Azamat Turgunbaev
 */

public class ApplicationLifeCycleListener implements ServletContextListener {

//    @EJB
//    private EqueueScheduler equeueScheduler;
//
    @Override
    public void contextInitialized(ServletContextEvent sce) {
//        equeueScheduler.update();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }

}
