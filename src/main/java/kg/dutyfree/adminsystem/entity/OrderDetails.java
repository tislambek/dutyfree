package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import kg.dutyfree.adminsystem.enums.OrdersEnum;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "order_details")
@Access(AccessType.PROPERTY)
public class OrderDetails extends AbstractEntity<Long> {
    @Setter
    private Integer orderId;
    @Setter
    private Date flightDate;
    @Setter
    private Date createdAt;
    @Setter
    private String fio;
    @Setter
    private String phone;
    @Setter
    private OrdersEnum status;

    @Column(name = "order_id")
    public Integer getOrderId() {
        return orderId;
    }

    @Column(name = "flight_date")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getFlightDate() {
        return flightDate;
    }

    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedAt() {
        return createdAt;
    }

    @Column(name = "user_fio")
    public String getFio() {
        return fio;
    }

    @Column(name = "user_phone")
    public String getPhone() {
        return phone;
    }

    @Column(name = "order_status",columnDefinition = "text default 'CREATED'")
    @Enumerated(EnumType.STRING)
    public OrdersEnum getStatus() {
        return status;
    }
}
