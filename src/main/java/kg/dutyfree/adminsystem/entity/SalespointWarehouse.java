package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "salespoint_warehouse")
@Access(AccessType.PROPERTY)
@ToString
public class SalespointWarehouse extends AbstractEntity<Long> {
    @Setter
    private Double price;
    @Setter
    private Integer discount;
    @Setter
    private Products products;
    @Setter
    private Salespoints salespoint;

    @Column(name = "product_price", nullable = true, precision = 0)
    public Double getPrice() {
        return price;
    }

    @Column(name = "product_discount", nullable = true)
    public Integer getDiscount() {
        return discount;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    public Products getProducts() {
        return products;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "salespoint_id", referencedColumnName = "id")
    public Salespoints getSalespoint() {
        return salespoint;
    }

}
