package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import kg.dutyfree.adminsystem.enums.OrdersEnum;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "orders")
@Access(AccessType.PROPERTY)
@ToString
public class Orders extends AbstractEntity<Long> {
    @Setter
    private Integer quantity;
    @Setter
    private Integer orderId;
    @Setter
    private Date createdAt;
    @Setter
    private Date updatedAt;
    @Setter
    private Date deletedAt;
    @Setter
    private SalespointWarehouse warehouse;
    @Setter
    private OrdersEnum status;

    @Column(name = "quantity", nullable = true)
    public Integer getQuantity() {
        return quantity;
    }

    @Column(name = "order_id", nullable = true)
    public Integer getOrderId() {
        return orderId;
    }

    @Column(name = "created_at", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedAt() {
        return createdAt;
    }

    @Column(name = "updated_at", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Column(name = "deleted_at", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDeletedAt() {
        return deletedAt;
    }

    @ManyToOne
    @JoinColumn(name = "sales_wh_id", referencedColumnName = "id")
    public SalespointWarehouse getWarehouse() {
        return warehouse;
    }


}
