package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "currencies")
@Access(AccessType.PROPERTY)
@ToString
public class Currencies extends AbstractEntity<Long> {
    @Setter
    private String name;
    @Setter
    private String code;

    @Column(name = "currency_name", nullable = true)
    public String getName() {
        return name;
    }

    @Column(name = "currency_code", nullable = true)
    public String getCode() {
        return code;
    }

}
