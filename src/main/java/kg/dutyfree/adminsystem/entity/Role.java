package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "roles")
@Access(AccessType.PROPERTY)
@ToString
public class Role extends AbstractEntity<Long> {
    @Setter
    private String title;
    @Setter
    private String description;

    @Column(name = "role_name", nullable = false)
    public String getTitle() {
        return title;
    }

    @Column(name = "role_description", nullable = false)
    public String getDescription() {
        return description;
    }
}
