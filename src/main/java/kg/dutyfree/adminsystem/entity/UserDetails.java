package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;

@Entity
@Table(name = "user_details")
@Access(AccessType.PROPERTY)
@ToString
public class UserDetails extends AbstractEntity<Long> {
    @Setter
    private String name;
    @Setter
    private String surname;
    @Setter
    private String email;
    @Setter
    private String phone;
    @Setter
    private Date birthDate;
    @Setter
    private String gender;

    @Column(name = "user_name", nullable = true)
    public String getName() {
        return name;
    }

    @Column(name = "user_surname", nullable = true)
    public String getSurname() {
        return surname;
    }

    @Column(name = "user_email", nullable = true)
    public String getEmail() {
        return email;
    }

    @Column(name = "user_phone", nullable = true)
    public String getPhone() {
        return phone;
    }

    @Column(name = "user_birthdate", nullable = true)
    public Date getBirthDate() {
        return birthDate;
    }

    @Column(name = "user_gender", nullable = true)
    public String getGender() {
        return gender;
    }

}
