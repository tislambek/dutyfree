package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import kg.dutyfree.adminsystem.enums.ShopType;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "tradepoints")
@Access(AccessType.PROPERTY)
@ToString
public class Tradepoints extends AbstractEntity<Long> {
    @Setter
    private String name;
    @Setter
    private ShopType type;
    @Setter
    private Boolean visible;
    @Setter
    private Country country;

    @Column(name = "tradepoint_name", nullable = true)
    public String getName() {
        return name;
    }

    @Column(name = "tradepoint_type")
    @Enumerated(EnumType.STRING)
    public ShopType getType() {
        return type;
    }

    @Column(name = "is_visible", nullable = true)
    public Boolean getVisible() {
        return visible;
    }

    @ManyToOne
    @JoinColumn(name = "country_id", referencedColumnName = "id")
    public Country getCountry() {
        return country;
    }

}
