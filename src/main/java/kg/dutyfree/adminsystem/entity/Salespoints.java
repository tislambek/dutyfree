package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "salespoints")
@Access(AccessType.PROPERTY)
@ToString
public class Salespoints extends AbstractEntity<Long> {
    @Setter
    private String name;
    @Setter
    private Boolean visible;
    @Setter
    private Tradepoints tradepoint;

    @Column(name = "salespoint_name", nullable = false)
    public String getName() {
        return name;
    }

    @Column(name = "is_visible", nullable = true)
    public Boolean getVisible() {
        return visible;
    }

    @ManyToOne
    @JoinColumn(name = "tradepoint_id", referencedColumnName = "id")
    public Tradepoints getTradepoint() {
        return tradepoint;
    }
}
