package kg.dutyfree.adminsystem.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "products")
@Access(AccessType.PROPERTY)
@ToString
@TypeDef(
        name = "jsonb",
        typeClass = JsonBinaryType.class
)
public class Products extends AbstractEntity<Long> {
    @Setter
    private String name;
    @Setter
    private String description;
    @Setter
    private String image;
    @Setter
    private String params;
    @Setter
    private Brands brands;
    @Setter
    private Category category;
    @Setter
    private Subcategories subcategory;

    @Column(name = "product_name", nullable = true)
    public String getName() {
        return name;
    }

    @Column(name = "product_description", nullable = true)
    public String getDescription() {
        return description;
    }

    @Column(name = "product_image")
    public String getImage() {
        return image;
    }

    @Column(name = "product_params", nullable = true,columnDefinition = "jsonb")
    @Type(type = "jsonb")
    public String getParams() {
        return params;
    }

    @ManyToOne
    @JoinColumn(name = "brand_id", referencedColumnName = "id")
    public Brands getBrands() {
        return brands;
    }

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    public Category getCategory() {
        return category;
    }

    @ManyToOne
    @JoinColumn(name = "subcategory_id", referencedColumnName = "id")
    public Subcategories getSubcategory() {
        return subcategory;
    }

}
