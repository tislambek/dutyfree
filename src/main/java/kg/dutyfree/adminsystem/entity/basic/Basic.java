package kg.dutyfree.adminsystem.entity.basic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Embeddable
@ToString
public class Basic {
    @Setter
    @NotNull
    private Date addDate;
    @Setter
    private Date changeDate;
    @Setter
    private Date deleteDate;
    @Setter
    private boolean deleted;

    public Date getAddDate() {
        return addDate;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Date getChangeDate() {
        return changeDate;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Date getDeleteDate() {
        return deleteDate;
    }

    @JsonIgnore
    public boolean isDeleted() {
        return deleted;
    }
}
