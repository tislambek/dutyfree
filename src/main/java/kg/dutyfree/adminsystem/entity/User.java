package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
@Access(AccessType.PROPERTY)
@ToString
public class User extends AbstractEntity<Long> {
    @Setter
    private String username;
    @Setter
    private String password;
    @Setter
    private String salt;
    @Setter
    private String status;
    @Setter
    private LocalDateTime createdAt;
    @Setter
    private LocalDateTime updatedAt;
    @Setter
    private LocalDateTime deletedAt;
    @Setter
    private Role role;

    @Column(name = "username", nullable = true)
    public String getUsername() {
        return username;
    }
    @Column(name = "password", nullable = true)
    public String getPassword() {
        return password;
    }
    @Column(name = "salt", nullable = true)
    public String getSalt() {
        return salt;
    }
    @Column(name = "user_status", nullable = true)
    public String getStatus() {
        return status;
    }
    @Column(name = "created_at", nullable = true)
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    @Column(name = "updated_at", nullable = true)
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }
    @Column(name = "deleted_at", nullable = true)
    public LocalDateTime getDeletedAt() {
        return deletedAt;
    }

    @ManyToOne
    @JoinColumn(name = "user_role", referencedColumnName = "id")
    public Role getRole() {
        return role;
    }

}
