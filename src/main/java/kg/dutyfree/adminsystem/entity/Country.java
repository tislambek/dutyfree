package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "countries")
@Access(AccessType.PROPERTY)
@ToString
public class Country extends AbstractEntity<Long> {
    @Setter
    private String name;
    @Setter
    private String description;
    @Setter
    private String image;

    @Column(name = "country_name", nullable = false)
    public String getName() {
        return name;
    }

    @Column(name = "country_description", nullable = true)
    public String getDescription() {
        return description;
    }

    @Column(name = "country_image")
    public String getImage() {
        return image;
    }

}