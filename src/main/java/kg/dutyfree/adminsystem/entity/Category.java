package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "categories")
@Access(AccessType.PROPERTY)
@ToString
public class Category extends AbstractEntity<Long> {
    @Setter
    private String name;
    @Setter
    private String description;
    @Setter
    private String image;

    @Column(name = "category_name")
    public String getName() {
        return name;
    }

    @Column(name = "category_description")
    public String getDescription() {
        return description;
    }

    @Column(name = "category_image")
    public String getImage() {
        return image;
    }
}
