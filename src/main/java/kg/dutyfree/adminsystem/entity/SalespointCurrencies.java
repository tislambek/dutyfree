package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "salespoint_currencies")
@Access(AccessType.PROPERTY)
@ToString
public class SalespointCurrencies extends AbstractEntity<Long> {
    @Setter
    private boolean common;
    @Setter
    private Currencies currency;
    @Setter
    private Salespoints salespoint;

    @Column(name = "is_default", nullable = true)
    public boolean isCommon() {
        return common;
    }

    @ManyToOne
    @JoinColumn(name = "currency_id", referencedColumnName = "id")
    public Currencies getCurrency() {
        return currency;
    }

    @ManyToOne
    @JoinColumn(name = "salespoint_id", referencedColumnName = "id")
    public Salespoints getSalespoint() {
        return salespoint;
    }
}
