package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "subcategories")
@Access(AccessType.PROPERTY)
@ToString
public class Subcategories extends AbstractEntity<Long> {
    @Setter
    private String name;
    @Setter
    private Category category;

    @Column(name = "subcategory_name", nullable = true)
    public String getName() {
        return name;
    }

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    public Category getCategory() {
        return category;
    }

}
