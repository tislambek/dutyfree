package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "banners")
@Access(AccessType.PROPERTY)
@ToString
public class Banners extends AbstractEntity<Long> {
    @Setter
    private String image;
    @Setter
    private String name;
    @Setter
    private boolean enable;
    @Setter
    private Category category;

    @Column(name = "banner_image", nullable = true)
    public String getImage() {
        return image;
    }

    @Column(name = "banner_name", nullable = true)
    public String getName() {
        return name;
    }

    @Column(name = "enable", nullable = true)
    public boolean isEnable() {
        return enable;
    }

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    public Category getCategory() {
        return category;
    }
}
