package kg.dutyfree.adminsystem.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "salespoint_details")
@Access(AccessType.PROPERTY)
@ToString
@TypeDef(
        name = "jsonb",
        typeClass = JsonBinaryType.class
)
public class SalespointDetails extends AbstractEntity<Long> {
    @Setter
    private String coordinates;
    @Setter
    private Salespoints salespoint;

    @Column(name = "salespoint_coordinates", nullable = true,columnDefinition = "jsonb")
    @Type(type = "jsonb")
    public String getCoordinates() {
        return coordinates;
    }

    @ManyToOne
    @JoinColumn(name = "salespoint_id", referencedColumnName = "id")
    public Salespoints getSalespoint() {
        return salespoint;
    }

}
