package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "brands")
@Access(AccessType.PROPERTY)
@ToString
public class Brands extends AbstractEntity<Long> {
    @Setter
    private String name;
    @Setter
    private String description;
    @Setter
    private String image;
    @Setter
    private Category category;

    @Column(name = "brand_name", nullable = true)
    public String getName() {
        return name;
    }

    @Column(name = "brand_description", nullable = true)
    public String getDescription() {
        return description;
    }

    @Column(name = "brand_image")
    public String getImage() {
        return image;
    }

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    public Category getCategory() {
        return category;
    }
}
