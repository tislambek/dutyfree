package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Entity
@Table(name = "shopping_cart")
@Access(AccessType.PROPERTY)
@ToString
public class ShoppingCart extends AbstractEntity<Long> {
    @Setter
    private Integer quantity;
    @Setter
    private LocalDateTime createdAt;
    @Setter
    private LocalDateTime updatedAt;
    @Setter
    private LocalDateTime deletedAt;
    @Setter
    private Salespoints salespoint;
    @Setter
    private Products product;
    @Setter
    private User user;

    @Column(name = "quantity", nullable = true)
    public Integer getQuantity() {
        return quantity;
    }

    @Column(name = "created_at", nullable = true)
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @Column(name = "updated_at", nullable = true)
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    @Column(name = "deleted_at", nullable = true)
    public LocalDateTime getDeletedAt() {
        return deletedAt;
    }

    @ManyToOne
    @JoinColumn(name = "salepoint_id", referencedColumnName = "id")
    public Salespoints getSalespoint() {
        return salespoint;
    }


    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    public Products getProduct() {
        return product;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public User getUser() {
        return user;
    }
}
