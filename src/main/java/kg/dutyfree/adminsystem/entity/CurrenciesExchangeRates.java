package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Entity
@Table(name = "currencies_exchange_rates")
@Access(AccessType.PROPERTY)
@ToString
public class CurrenciesExchangeRates extends AbstractEntity<Long> {
    @Setter
    private Double exchangeRate;
    @Setter
    private LocalDateTime createdAt;
    @Setter
    private Currencies from;
    @Setter
    private Currencies to;

    @Column(name = "exchange_rate", nullable = true, precision = 0)
    public Double getExchangeRate() {
        return exchangeRate;
    }

    @Column(name = "created_at", nullable = true)
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "from_currency_id", referencedColumnName = "id")
    public Currencies getFrom() {
        return from;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "to_currency_id", referencedColumnName = "id")
    public Currencies getTo() {
        return to;
    }

}
