package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "promo_sales")
@Access(AccessType.PROPERTY)
@ToString
public class PromoSales extends AbstractEntity<Long> {
    @Setter
    private SalespointWarehouse warehouse;

    @ManyToOne
    @JoinColumn(name = "warehouse_id", referencedColumnName = "id")
    public SalespointWarehouse getWarehouse() {
        return warehouse;
    }
}
