package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "user_settings")
@Access(AccessType.PROPERTY)
@ToString
public class UserSettings extends AbstractEntity<Long> {
    @Setter
    private Boolean subscription;
    @Setter
    private Salespoints salespoint;
    @Setter
    private Currencies currency;

    @Column(name = "user_subscription", nullable = true)
    public Boolean getSubscription() {
        return subscription;
    }

    @ManyToOne
    @JoinColumn(name = "user_salespoint", referencedColumnName = "id")
    public Salespoints getSalespoint() {
        return salespoint;
    }

    @ManyToOne
    @JoinColumn(name = "user_currency", referencedColumnName = "id")
    public Currencies getCurrency() {
        return currency;
    }

}
