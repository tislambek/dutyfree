package kg.dutyfree.adminsystem.entity;

import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.LocalDateTime;

@Entity
@Table(name = "user_preferences")
@Access(AccessType.PROPERTY)
@ToString
public class UserPreferences extends AbstractEntity<Long> {
    @Setter
    private LocalDateTime createdAt;
    @Setter
    private Products products;
    @Setter
    private User user;

    @Column(name = "created_at", nullable = true)
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    public Products getProducts() {
        return products;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public User getUser() {
        return user;
    }
}
