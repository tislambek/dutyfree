package kg.dutyfree.adminsystem.enums;

/****
 * @author aziko
 */
public enum InequalityConstants {

    EQUAL,
    NOT_EQUAL,
    NOT,
    GREATER,
    LESSER,
    INTERVAL,
    LIKE,
    IN,
    NOT_IN,
    IS_NULL,
    IS_NOT_NULL,
    IS_NULL_SINGLE,
    MEMBER_OF,
    IS_NOT_NULL_SINGLE,
    IS_EMPTY,
    IS_NOT_EMPTY,
    LESSER_EQUAL,
    GREATER_EQUAL,
}
