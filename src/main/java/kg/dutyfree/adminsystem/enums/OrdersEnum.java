package kg.dutyfree.adminsystem.enums;

public enum OrdersEnum {
    CREATED,
    ORDERED,
    CONFIRMED,
    CANCELED,
    EXPIRED
}
