package kg.dutyfree.adminsystem.enums;

/****
 * @author aziko
 */

public enum SortEnum {

    ASCENDING,
    DESCENDING

}
