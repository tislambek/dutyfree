package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.ShoppingCart;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface ShoppingCartService extends GenericService<ShoppingCart, Long> {
}
