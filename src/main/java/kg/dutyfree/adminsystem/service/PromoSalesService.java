package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.PromoSales;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface PromoSalesService extends GenericService<PromoSales, Long>{
    void commit();
}
