package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Country;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface CountryService extends GenericService<Country, Long>{
}
