package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Banners;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface BannersService extends GenericService<Banners, Long>{
}
