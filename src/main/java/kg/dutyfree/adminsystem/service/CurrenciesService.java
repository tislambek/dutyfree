package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Currencies;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface CurrenciesService extends GenericService<Currencies, Long> {
}
