package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.OrderDetails;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface OrderDetailsService extends GenericService<OrderDetails, Long> {
}
