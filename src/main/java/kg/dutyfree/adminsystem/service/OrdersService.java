package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Orders;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface OrdersService extends GenericService<Orders, Long> {
}
