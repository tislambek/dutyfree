package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.TopSales;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface TopSalesService extends GenericService<TopSales, Long>{
    void commit();
}
