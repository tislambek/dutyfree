package kg.dutyfree.adminsystem.service;


import kg.dutyfree.adminsystem.entity.User;

import javax.ejb.Local;
import java.math.BigInteger;

/****
 * @author aziko
 */

@Local
public interface UserService extends GenericService<User, Long> {

}
