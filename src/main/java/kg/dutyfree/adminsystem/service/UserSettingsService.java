package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.UserSettings;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface UserSettingsService extends GenericService<UserSettings, Long>{
}
