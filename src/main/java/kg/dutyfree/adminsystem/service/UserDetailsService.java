package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.UserDetails;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface UserDetailsService extends GenericService<UserDetails, Long> {
}
