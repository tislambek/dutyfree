package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Subcategories;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface SubcategoriesService extends GenericService<Subcategories, Long>{
}
