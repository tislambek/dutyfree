package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.SubcategoriesDao;
import kg.dutyfree.adminsystem.dao.impl.SubcategoriesDaoImpl;
import kg.dutyfree.adminsystem.entity.Subcategories;
import kg.dutyfree.adminsystem.service.SubcategoriesService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SubcategoriesServiceImpl
        extends GenericServiceImpl<Subcategories, Long, SubcategoriesDao>
        implements SubcategoriesService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private SubcategoriesDao dao;

    @PostConstruct
    public void initialize() {
        dao = new SubcategoriesDaoImpl(em);
    }
    @Override
    protected SubcategoriesDao getDao() {
        return dao;
    }
}
