package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.UserDetailsDao;
import kg.dutyfree.adminsystem.dao.impl.UserDetailsDaoImpl;
import kg.dutyfree.adminsystem.entity.UserDetails;
import kg.dutyfree.adminsystem.service.UserDetailsService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserDetailsServiceImpl
        extends GenericServiceImpl<UserDetails, Long, UserDetailsDao>
        implements UserDetailsService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private UserDetailsDao dao;

    @PostConstruct
    public void initialize() {
        dao = new UserDetailsDaoImpl(em);
    }
    @Override
    protected UserDetailsDao getDao() {
        return dao;
    }
}
