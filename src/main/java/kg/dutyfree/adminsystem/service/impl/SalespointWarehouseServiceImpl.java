package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.SalespointWarehouseDao;
import kg.dutyfree.adminsystem.dao.impl.SalespointWarehouseDaoImpl;
import kg.dutyfree.adminsystem.entity.SalespointWarehouse;
import kg.dutyfree.adminsystem.service.SalespointWarehouseService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SalespointWarehouseServiceImpl
        extends GenericServiceImpl<SalespointWarehouse, Long, SalespointWarehouseDao>
        implements SalespointWarehouseService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private SalespointWarehouseDao dao;

    @PostConstruct
    public void initialize() {
        dao = new SalespointWarehouseDaoImpl(em);
    }
    @Override
    protected SalespointWarehouseDao getDao() {
        return dao;
    }
}
