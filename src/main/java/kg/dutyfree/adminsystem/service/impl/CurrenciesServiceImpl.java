package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.CurrenciesDao;
import kg.dutyfree.adminsystem.dao.impl.CurrenciesDaoImpl;
import kg.dutyfree.adminsystem.entity.Currencies;
import kg.dutyfree.adminsystem.service.CurrenciesService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CurrenciesServiceImpl extends GenericServiceImpl<Currencies, Long, CurrenciesDao> implements CurrenciesService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private CurrenciesDao dao;

    @PostConstruct
    public void initialize() {
        dao = new CurrenciesDaoImpl(em);
    }
    @Override
    protected CurrenciesDao getDao() {
        return dao;
    }
}
