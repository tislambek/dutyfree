package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.CurrenciesExchangeRatesDao;
import kg.dutyfree.adminsystem.dao.impl.CurrenciesExchangeRatesDaoImpl;
import kg.dutyfree.adminsystem.entity.CurrenciesExchangeRates;
import kg.dutyfree.adminsystem.service.CurrenciesExchangeRatesService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CurrenciesExchangeRatesServiceImpl extends GenericServiceImpl<CurrenciesExchangeRates, Long, CurrenciesExchangeRatesDao> implements CurrenciesExchangeRatesService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private CurrenciesExchangeRatesDao dao;

    @PostConstruct
    public void initialize() {
        dao = new CurrenciesExchangeRatesDaoImpl(em);
    }
    @Override
    protected CurrenciesExchangeRatesDao getDao() {
        return dao;
    }
}
