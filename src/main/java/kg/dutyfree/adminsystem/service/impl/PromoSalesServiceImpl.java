package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.PromoSalesDao;
import kg.dutyfree.adminsystem.dao.impl.PromoSalesDaoImpl;
import kg.dutyfree.adminsystem.entity.PromoSales;
import kg.dutyfree.adminsystem.service.PromoSalesService;
import kg.dutyfree.adminsystem.service.impl.GenericServiceImpl;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PromoSalesServiceImpl extends GenericServiceImpl<PromoSales, Long, PromoSalesDao> implements PromoSalesService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private PromoSalesDao dao;

    @PostConstruct
    public void initialize() {
        dao = new PromoSalesDaoImpl(em);
    }

    @Override
    protected PromoSalesDao getDao() {
        return dao;
    }

    @Override
    public void commit() {
        em.getTransaction().commit();
    }
}
