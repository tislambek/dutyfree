package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.SalespointDetailsDao;
import kg.dutyfree.adminsystem.dao.impl.SalespointDetailsDaoImpl;
import kg.dutyfree.adminsystem.entity.SalespointDetails;
import kg.dutyfree.adminsystem.service.SalespointDetailsService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SalespointDetailsServiceimpl
        extends GenericServiceImpl<SalespointDetails, Long, SalespointDetailsDao>
        implements SalespointDetailsService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private SalespointDetailsDao dao;

    @PostConstruct
    public void initialize() {
        dao = new SalespointDetailsDaoImpl(em);
    }
    @Override
    protected SalespointDetailsDao getDao() {
        return dao;
    }
}
