package kg.dutyfree.adminsystem.service.impl;


import kg.dutyfree.adminsystem.entity.User;
import kg.dutyfree.adminsystem.service.UserService;
import kg.dutyfree.adminsystem.dao.UserDao;
import kg.dutyfree.adminsystem.dao.impl.UserDaoImpl;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

/**
 * @author aziko
 */

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserServiceImpl extends GenericServiceImpl<User, Long, UserDao> implements UserService {

    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private UserDao dao;

    @PostConstruct
    public void initialize() {
        dao = new UserDaoImpl(em);
    }

    @Override
    protected UserDao getDao() {
        return dao;
    }
}
