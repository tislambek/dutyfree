package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.ProductsDao;
import kg.dutyfree.adminsystem.dao.impl.ProductsDaoImpl;
import kg.dutyfree.adminsystem.entity.Products;
import kg.dutyfree.adminsystem.service.ProductsService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ProductsServiceImpl extends GenericServiceImpl<Products, Long, ProductsDao> implements ProductsService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private ProductsDao dao;

    @PostConstruct
    public void initialize() {
        dao = new ProductsDaoImpl(em);
    }
    @Override
    protected ProductsDao getDao() {
        return dao;
    }
}
