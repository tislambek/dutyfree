package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.RoleDao;
import kg.dutyfree.adminsystem.dao.impl.RoleDaoImpl;
import kg.dutyfree.adminsystem.entity.Role;
import kg.dutyfree.adminsystem.service.RoleService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

/**
 * Created by kbakytbekov on 07.09.2017.
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class RoleServiceImpl extends GenericServiceImpl<Role, Long,RoleDao> implements RoleService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private RoleDao dao;

    @PostConstruct
    public void initialize() {
        dao = new RoleDaoImpl(em);
    }

    @Override
    protected RoleDao getDao() {
        return dao;
    }
}
