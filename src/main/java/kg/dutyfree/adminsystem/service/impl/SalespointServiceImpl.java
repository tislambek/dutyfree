package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.SalespointsDao;
import kg.dutyfree.adminsystem.dao.impl.SalespointsDaoImpl;
import kg.dutyfree.adminsystem.entity.Salespoints;
import kg.dutyfree.adminsystem.service.SalespointsService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SalespointServiceImpl extends GenericServiceImpl<Salespoints, Long, SalespointsDao> implements SalespointsService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private SalespointsDao dao;

    @PostConstruct
    public void initialize() {
        dao = new SalespointsDaoImpl(em);
    }
    @Override
    protected SalespointsDao getDao() {
        return dao;
    }
}
