package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.UserSettingsDao;
import kg.dutyfree.adminsystem.dao.impl.UserSettingsDaoImpl;
import kg.dutyfree.adminsystem.entity.UserSettings;
import kg.dutyfree.adminsystem.service.UserSettingsService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserSettingsServiceImpl
        extends GenericServiceImpl<UserSettings, Long, UserSettingsDao>
        implements UserSettingsService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private UserSettingsDao dao;

    @PostConstruct
    public void initialize() {
        dao = new UserSettingsDaoImpl(em);
    }
    @Override
    protected UserSettingsDao getDao() {
        return dao;
    }
}
