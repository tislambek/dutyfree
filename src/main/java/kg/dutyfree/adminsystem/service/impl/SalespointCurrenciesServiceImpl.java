package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.SalespointCurrenciesDao;
import kg.dutyfree.adminsystem.dao.impl.SalespointCurrenciesDaoImpl;
import kg.dutyfree.adminsystem.entity.SalespointCurrencies;
import kg.dutyfree.adminsystem.service.SalespointCurrenciesService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SalespointCurrenciesServiceImpl
        extends GenericServiceImpl<SalespointCurrencies, Long, SalespointCurrenciesDao>
        implements SalespointCurrenciesService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private SalespointCurrenciesDao dao;

    @PostConstruct
    public void initialize() {
        dao = new SalespointCurrenciesDaoImpl(em);
    }
    @Override
    protected SalespointCurrenciesDao getDao() {
        return dao;
    }
}
