package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.ShoppingCartDao;
import kg.dutyfree.adminsystem.dao.impl.ShoppingCartDaoImpl;
import kg.dutyfree.adminsystem.entity.ShoppingCart;
import kg.dutyfree.adminsystem.service.ShoppingCartService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ShoppingCartServiceImpl
        extends GenericServiceImpl<ShoppingCart, Long, ShoppingCartDao>
        implements ShoppingCartService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private ShoppingCartDao dao;

    @PostConstruct
    public void initialize() {
        dao = new ShoppingCartDaoImpl(em);
    }
    @Override
    protected ShoppingCartDao getDao() {
        return dao;
    }
}
