package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.CountryDao;
import kg.dutyfree.adminsystem.dao.impl.CountryDaoImpl;
import kg.dutyfree.adminsystem.entity.Country;
import kg.dutyfree.adminsystem.service.CountryService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CountryServiceImpl extends GenericServiceImpl<Country, Long, CountryDao> implements CountryService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private CountryDao dao;

    @PostConstruct
    public void initialize() {
        dao = new CountryDaoImpl(em);
    }
    @Override
    protected CountryDao getDao() {
        return dao;
    }
}
