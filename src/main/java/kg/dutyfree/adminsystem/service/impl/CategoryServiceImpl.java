package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.CategoryDao;
import kg.dutyfree.adminsystem.dao.impl.CategoryDaoImpl;
import kg.dutyfree.adminsystem.entity.Category;
import kg.dutyfree.adminsystem.service.CategoryService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CategoryServiceImpl extends GenericServiceImpl<Category, Long, CategoryDao> implements CategoryService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private CategoryDao dao;

    @PostConstruct
    public void initialize() {
        dao = new CategoryDaoImpl(em);
    }
    @Override
    protected CategoryDao getDao() {
        return dao;
    }
}
