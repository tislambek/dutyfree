package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.BrandsDao;
import kg.dutyfree.adminsystem.dao.impl.BrandsDaoImpl;
import kg.dutyfree.adminsystem.entity.Brands;
import kg.dutyfree.adminsystem.service.BrandsService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BrandsServiceImpl extends GenericServiceImpl<Brands, Long, BrandsDao> implements BrandsService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private BrandsDao dao;

    @PostConstruct
    public void initialize() {
        dao = new BrandsDaoImpl(em);
    }
    @Override
    protected BrandsDao getDao() {
        return dao;
    }
}
