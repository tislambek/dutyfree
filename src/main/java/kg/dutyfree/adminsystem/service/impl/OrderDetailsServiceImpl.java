package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.OrderDetailsDao;
import kg.dutyfree.adminsystem.dao.impl.OrderDetailsDaoImpl;
import kg.dutyfree.adminsystem.entity.OrderDetails;
import kg.dutyfree.adminsystem.service.OrderDetailsService;
import kg.dutyfree.adminsystem.service.impl.GenericServiceImpl;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class OrderDetailsServiceImpl extends GenericServiceImpl<OrderDetails, Long, OrderDetailsDao> implements OrderDetailsService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private OrderDetailsDao dao;

    @PostConstruct
    public void initialize() {
        dao = new OrderDetailsDaoImpl(em);
    }

    @Override
    protected OrderDetailsDao getDao() {
        return dao;
    }
}
