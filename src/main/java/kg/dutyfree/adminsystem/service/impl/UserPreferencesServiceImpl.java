package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.UserPreferencesDao;
import kg.dutyfree.adminsystem.dao.impl.UserPreferencesDaoImpl;
import kg.dutyfree.adminsystem.entity.UserPreferences;
import kg.dutyfree.adminsystem.service.UserPreferencesService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class UserPreferencesServiceImpl
        extends GenericServiceImpl<UserPreferences, Long, UserPreferencesDao>
        implements UserPreferencesService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private UserPreferencesDao dao;

    @PostConstruct
    public void initialize() {
        dao = new UserPreferencesDaoImpl(em);
    }
    @Override
    protected UserPreferencesDao getDao() {
        return dao;
    }
}
