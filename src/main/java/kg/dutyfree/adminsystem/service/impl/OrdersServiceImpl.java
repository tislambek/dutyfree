package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.OrdersDao;
import kg.dutyfree.adminsystem.dao.impl.OrdersDaoImpl;
import kg.dutyfree.adminsystem.entity.Orders;
import kg.dutyfree.adminsystem.service.OrdersService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class OrdersServiceImpl extends GenericServiceImpl<Orders, Long, OrdersDao> implements OrdersService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private OrdersDao dao;

    @PostConstruct
    public void initialize() {
        dao = new OrdersDaoImpl(em);
    }
    @Override
    protected OrdersDao getDao() {
        return dao;
    }
}
