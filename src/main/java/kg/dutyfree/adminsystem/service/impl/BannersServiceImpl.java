package kg.dutyfree.adminsystem.service.impl;


import kg.dutyfree.adminsystem.dao.BannersDao;
import kg.dutyfree.adminsystem.dao.BrandsDao;
import kg.dutyfree.adminsystem.dao.impl.BannersDaoImpl;
import kg.dutyfree.adminsystem.dao.impl.BrandsDaoImpl;
import kg.dutyfree.adminsystem.entity.Banners;
import kg.dutyfree.adminsystem.service.BannersService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BannersServiceImpl extends GenericServiceImpl<Banners, Long, BannersDao> implements BannersService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private BannersDao dao;

    @PostConstruct
    public void initialize() {
        dao = new BannersDaoImpl(em);
    }

    @Override
    protected BannersDao getDao() {
        return dao;
    }
}
