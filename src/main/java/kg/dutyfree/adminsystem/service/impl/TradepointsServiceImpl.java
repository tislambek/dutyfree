package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.TradepointsDao;
import kg.dutyfree.adminsystem.dao.impl.TradepointsDaoImpl;
import kg.dutyfree.adminsystem.entity.Tradepoints;
import kg.dutyfree.adminsystem.service.TradepointsService;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TradepointsServiceImpl extends GenericServiceImpl<Tradepoints, Long, TradepointsDao>
        implements TradepointsService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private TradepointsDao dao;

    @PostConstruct
    public void initialize() {
        dao = new TradepointsDaoImpl(em);
    }
    @Override
    protected TradepointsDao getDao() {
        return dao;
    }
}
