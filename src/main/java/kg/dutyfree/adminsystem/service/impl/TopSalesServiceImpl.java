package kg.dutyfree.adminsystem.service.impl;

import kg.dutyfree.adminsystem.dao.TopSalesDao;
import kg.dutyfree.adminsystem.dao.impl.TopSalesDaoImpl;
import kg.dutyfree.adminsystem.entity.TopSales;
import kg.dutyfree.adminsystem.service.TopSalesService;
import kg.dutyfree.adminsystem.service.impl.GenericServiceImpl;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.math.BigInteger;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class TopSalesServiceImpl extends GenericServiceImpl<TopSales, Long, TopSalesDao> implements TopSalesService {
    @PersistenceContext(type = PersistenceContextType.TRANSACTION)
    private EntityManager em;
    private TopSalesDao dao;

    @PostConstruct
    public void initialize() {
        dao = new TopSalesDaoImpl(em);
    }
    @Override
    protected TopSalesDao getDao() {
        return dao;
    }

    @Override
    public void commit() {
        em.getTransaction().commit();
    }
}
