package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Category;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface CategoryService extends GenericService<Category, Long> {
}
