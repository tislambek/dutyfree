package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Role;

import javax.ejb.Local;
import java.math.BigInteger;

/**
 * Created by kbakytbekov on 07.09.2017.
 */
@Local
public interface RoleService extends GenericService<Role, Long>{
}
