package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.SalespointWarehouse;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface SalespointWarehouseService extends GenericService<SalespointWarehouse, Long> {
}
