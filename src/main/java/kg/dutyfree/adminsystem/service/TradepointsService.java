package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Tradepoints;
import kg.dutyfree.adminsystem.service.GenericService;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface TradepointsService extends GenericService<Tradepoints, Long> {
}
