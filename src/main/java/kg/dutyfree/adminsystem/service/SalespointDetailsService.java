package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.SalespointDetails;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface SalespointDetailsService extends GenericService<SalespointDetails, Long>{
}
