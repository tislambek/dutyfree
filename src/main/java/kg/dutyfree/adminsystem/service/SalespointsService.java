package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Salespoints;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface SalespointsService extends GenericService<Salespoints, Long>{
}
