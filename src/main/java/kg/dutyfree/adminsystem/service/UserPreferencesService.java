package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.UserPreferences;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface UserPreferencesService extends GenericService<UserPreferences, Long>{
}
