package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Products;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface ProductsService extends GenericService<Products, Long> {
}
