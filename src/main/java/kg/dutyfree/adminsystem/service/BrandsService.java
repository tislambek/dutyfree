package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.Brands;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface BrandsService extends GenericService<Brands, Long> {
}
