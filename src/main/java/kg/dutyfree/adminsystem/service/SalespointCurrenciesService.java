package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.SalespointCurrencies;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface SalespointCurrenciesService extends GenericService<SalespointCurrencies, Long> {
}
