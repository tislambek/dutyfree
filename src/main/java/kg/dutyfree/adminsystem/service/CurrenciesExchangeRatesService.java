package kg.dutyfree.adminsystem.service;

import kg.dutyfree.adminsystem.entity.CurrenciesExchangeRates;

import javax.ejb.Local;
import java.math.BigInteger;

@Local
public interface CurrenciesExchangeRatesService extends GenericService<CurrenciesExchangeRates, Long>{
}
