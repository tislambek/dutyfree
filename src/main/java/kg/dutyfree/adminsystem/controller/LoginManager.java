package kg.dutyfree.adminsystem.controller;


import kg.dutyfree.adminsystem.annotation.RolesAllowed;
import kg.dutyfree.adminsystem.entity.User;
import kg.dutyfree.adminsystem.util.LoginUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.io.Serializable;

/**
 * Created by Калыс on 11/9/2017.
 */
@ManagedBean
@SessionScoped
public class LoginManager implements Serializable {

    @Inject
    private LoginUtil loginUtil;

    @RolesAllowed(roles = {"admin", "moderator", "user"})
    public String logout() {
        loginUtil.logout();
        return "/index.xhtml?faces-redirect=true";
    }

    public boolean hasRole(String role) {
        User user = loginUtil.getCurrentUser();
        return user != null && loginUtil.userHasRole(user, role);
    }

    public User getCurrentUser() {
        return loginUtil.getCurrentUser();
    }

    public String getCurrentUserRole() {
        if (getCurrentUser() != null) {
            getCurrentUser().getRole().getTitle();
        }
        return null;
    }
}
