package kg.dutyfree.adminsystem.controller.admin;

import kg.dutyfree.adminsystem.beans.FilterExample;
import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.*;
import kg.dutyfree.adminsystem.enums.InequalityConstants;
import kg.dutyfree.adminsystem.enums.SortEnum;
import kg.dutyfree.adminsystem.service.*;
import lombok.Getter;
import lombok.Setter;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@SessionScoped
public class SalespointWarehouseBean extends BaseBean<SalespointWarehouse> {
    @EJB
    private SalespointWarehouseService service;
    @EJB
    private SalespointsService salespointsService;
    @EJB
    private TopSalesService topSalesService;
    @EJB
    private PromoSalesService promoSalesService;

    @Setter
    @Getter
    private Products products;

    @Getter
    @Setter
    private boolean promo;
    @Getter
    @Setter
    private boolean top;

    @Override
    public String add() {
        setEntity(new SalespointWarehouse());
        return super.add();
    }

    public void addEntity() {
        System.out.println(entity);
        entity.setProducts(products);
        SalespointWarehouse merge = service.merge(entity);
        if (promo) {
            PromoSales promoSales=new PromoSales();
            promoSales.setWarehouse(merge);
            promoSalesService.persist(promoSales);
        }
        if (top) {
            TopSales topSales=new TopSales();
            topSales.setWarehouse(merge);
            topSalesService.persist(topSales);
        }
    }

    public String warehouse(Products products){
        setEntity(new SalespointWarehouse());
        this.products=products;
        return navigate("warehouse.xhtml");
    }

    public void init(){
        List<FilterExample> filters=new ArrayList<>();
        filters.add(new FilterExample("products",products, InequalityConstants.EQUAL));
        list= service.findByExample(0,0, SortEnum.ASCENDING,filters,"id");
        top=false;
        promo=false;
    }

    @Override
    public String edit(SalespointWarehouse edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(SalespointWarehouse delete) {
//        List<TopSales> warehouseTop = topSalesService.findByProperty("warehouse", delete);
//        if (!warehouseTop.isEmpty()){
//            topSalesService.remove(warehouseTop.get(0));
////            topSalesService.commit();
//        }
//        List<PromoSales> warehousePromo = promoSalesService.findByProperty("warehouse", delete);
//        if (!warehousePromo.isEmpty()){
//            promoSalesService.remove(warehousePromo.get(0));
////            promoSalesService.commit();
//        }

        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }

    public boolean getTopSales(SalespointWarehouse warehouse){
        return !topSalesService.findByProperty("warehouse",warehouse).isEmpty();
    }
    public boolean getPromoSales(SalespointWarehouse warehouse){
        return !promoSalesService.findByProperty("warehouse",warehouse).isEmpty();
    }

    public List<Salespoints> getSalespoints(){
        List<Salespoints> all = salespointsService.findAll();
        list.forEach(e->{
            all.remove(e.getSalespoint());
        });
        return all;
    }
}
