package kg.dutyfree.adminsystem.controller.admin;

import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Currencies;
import kg.dutyfree.adminsystem.service.CurrenciesService;
import kg.dutyfree.adminsystem.service.CurrenciesService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.UploadedFile;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class CurrenciesBean extends BaseBean<Currencies> {
    @EJB
    private CurrenciesService service;
    @Override
    public String add() {
        setEntity(new Currencies());
        return super.add();
    }

    public void init(){
//        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"basic.addDate");
        list= service.findAll();
    }

    @Override
    public String edit(Currencies edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(Currencies delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }
}
