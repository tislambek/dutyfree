package kg.dutyfree.adminsystem.controller.admin;

import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Banners;
import kg.dutyfree.adminsystem.entity.Category;
import kg.dutyfree.adminsystem.service.BannersService;
import kg.dutyfree.adminsystem.service.CategoryService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.UploadedFile;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

@Named
@SessionScoped
public class BannersBean extends BaseBean<Banners> {
    @EJB
    private BannersService service;
    @EJB
    private CategoryService categoryService;
    @Override
    public String add() {
        setEntity(new Banners());
        return super.add();
    }

    public String upload(Banners edit) {
        entity = edit;
        return navigate("upload.xhtml");
    }

    @Setter
    @Getter
    private UploadedFile file;

    public String uploadFile() {
        String filePath = this.uploadFile("banners/", file);
        if (filePath!=null){
            entity.setImage(filePath);
            service.merge(entity);
            System.out.println("Successful" + file.getFileName() + " is uploaded.");
            entity=null;
            return navigate("list.xhtml");
        }
        return null;
    }

    public void init(){
//        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"basic.addDate");
        list= service.findAll();
    }

    @Override
    public String edit(Banners edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(Banners delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            basic.setAddDate(new Date());
//            entity.setBasic(basic);
            entity=service.persist(entity);
        }else{
//            entity.getBasic().setChangeDate(new Date());
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }

    public List<Category> getCategories(){
        return categoryService.findAll();
    }
}
