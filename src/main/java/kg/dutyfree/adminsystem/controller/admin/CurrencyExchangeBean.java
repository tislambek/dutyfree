package kg.dutyfree.adminsystem.controller.admin;

import kg.dutyfree.adminsystem.beans.FilterExample;
import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Currencies;
import kg.dutyfree.adminsystem.entity.CurrenciesExchangeRates;
import kg.dutyfree.adminsystem.enums.InequalityConstants;
import kg.dutyfree.adminsystem.enums.SortEnum;
import kg.dutyfree.adminsystem.service.CurrenciesExchangeRatesService;
import kg.dutyfree.adminsystem.service.CurrenciesService;
import lombok.Getter;
import lombok.Setter;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Named
@SessionScoped
public class CurrencyExchangeBean extends BaseBean<CurrenciesExchangeRates> {
    @EJB
    private CurrenciesExchangeRatesService service;
    @EJB
    private CurrenciesService currenciesService;

    private Currencies currency;

    @Getter
    @Setter
    private Currencies to;
    @Getter
    @Setter
    private Double rate;

    @Override
    public String add() {
        setEntity(new CurrenciesExchangeRates());
        return super.add();
    }

    public List<CurrenciesExchangeRates> getRates(Currencies currencies){
        List<FilterExample> filters=new ArrayList<>();
        filters.add(new FilterExample("from",currencies,InequalityConstants.EQUAL));
        return service.findByExample(0,0,SortEnum.ASCENDING,filters,"id");
    }

    public String exchange(Currencies currency) {
        setEntity(new CurrenciesExchangeRates());
        this.currency=currency;
        System.out.println(entity);
        return navigate("exchange.xhtml");
    }

    public void addEntity() {
        entity.setCreatedAt(LocalDateTime.now());
        entity.setFrom(currency);
        System.out.println(entity);
        service.merge(entity);
    }

    @Override
    protected List<FilterExample> getFilters() {
        List<FilterExample> filters = super.getFilters();
        filters.add(new FilterExample("from",currency,InequalityConstants.EQUAL));
        return filters;
    }

    public void init(){
        list=service.findByExample(0,0, SortEnum.ASCENDING,getFilters(),"id");
//        list= service.findAll();
    }

    @Override
    public String edit(CurrenciesExchangeRates edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(CurrenciesExchangeRates delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity.setCreatedAt(LocalDateTime.now());
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }



    public List<Currencies> getCurrenciesTo(){
        List<Currencies> currencies= currenciesService.findAll();
        list.forEach(e->{
            currencies.remove(e.getTo());
        });
        currencies.remove(currency);
        return currencies;
    }
}
