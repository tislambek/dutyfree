package kg.dutyfree.adminsystem.controller.admin;

import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Salespoints;
import kg.dutyfree.adminsystem.entity.Tradepoints;
import kg.dutyfree.adminsystem.service.SalespointsService;
import kg.dutyfree.adminsystem.service.SalespointsService;
import kg.dutyfree.adminsystem.service.TradepointsService;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.List;

@Named
@SessionScoped
public class SalespointBean extends BaseBean<Salespoints> {
    @EJB
    private SalespointsService service;
    @EJB
    private TradepointsService tradepointsService;
    @Override
    public String add() {
        setEntity(new Salespoints());
        return super.add();
    }

    public void init(){
//        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"basic.addDate");
        list= service.findAll();
    }

    @Override
    public String edit(Salespoints edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(Salespoints delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }

    public List<Tradepoints> getTradepoints(){
        return tradepointsService.findAll();
    }
}
