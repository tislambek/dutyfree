package kg.dutyfree.adminsystem.controller.admin;

import com.google.gson.JsonObject;
import kg.dutyfree.adminsystem.beans.FilterExample;
import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Currencies;
import kg.dutyfree.adminsystem.entity.SalespointCurrencies;
import kg.dutyfree.adminsystem.entity.SalespointDetails;
import kg.dutyfree.adminsystem.entity.Salespoints;
import kg.dutyfree.adminsystem.enums.InequalityConstants;
import kg.dutyfree.adminsystem.enums.SortEnum;
import kg.dutyfree.adminsystem.service.CurrenciesService;
import kg.dutyfree.adminsystem.service.SalespointCurrenciesService;
import kg.dutyfree.adminsystem.service.SalespointDetailsService;
import kg.dutyfree.adminsystem.service.SalespointsService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@SessionScoped
public class SalespointCurrencyBean extends BaseBean<SalespointCurrencies> {
    @EJB
    private SalespointCurrenciesService service;
    @EJB
    private CurrenciesService currenciesService;

    @Setter
    @Getter
    private Salespoints salespoints;


    @Override
    public String add() {
        setEntity(new SalespointCurrencies());
        return super.add();
    }

    public void addEntity() {
        entity.setSalespoint(salespoints);
        service.merge(entity);
    }

    public String currecncy(Salespoints salespoints){
        setEntity(new SalespointCurrencies());
        this.salespoints=salespoints;
        return navigate("currency.xhtml");
    }

    public void init(){
        List<FilterExample> filters=new ArrayList<>();
        filters.add(new FilterExample("salespoint",salespoints, InequalityConstants.EQUAL));
        list= service.findByExample(0,0, SortEnum.ASCENDING,filters,"id");
    }

    @Override
    public String edit(SalespointCurrencies edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(SalespointCurrencies delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity.setSalespoint(salespoints);
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }

    public List<Currencies> getCurrencies(){
        List<Currencies> currencies = currenciesService.findAll();
        List<SalespointCurrencies> salespointCurrencies = service.findByProperty("salespoint",salespoints);
        salespointCurrencies.forEach(e->{
            currencies.remove(e.getCurrency());
        });
        return currencies;
    }
}
