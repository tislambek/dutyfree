package kg.dutyfree.adminsystem.controller.admin;


import kg.dutyfree.adminsystem.beans.FilterExample;
import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.OrderDetails;
import kg.dutyfree.adminsystem.entity.Orders;
import kg.dutyfree.adminsystem.entity.Products;
import kg.dutyfree.adminsystem.entity.Salespoints;
import kg.dutyfree.adminsystem.enums.OrdersEnum;
import kg.dutyfree.adminsystem.enums.SortEnum;
import kg.dutyfree.adminsystem.model.OrderModel;
import kg.dutyfree.adminsystem.service.OrderDetailsService;
import kg.dutyfree.adminsystem.service.OrdersService;
import kg.dutyfree.adminsystem.service.ProductsService;
import kg.dutyfree.adminsystem.service.SalespointsService;
import lombok.Getter;
import lombok.Setter;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author aziko
 */

@Named
@SessionScoped
public class OrdersBean extends BaseBean<Orders> {
    @EJB
    private OrdersService service;
    @EJB
    private SalespointsService salespointsService;
    @EJB
    private ProductsService productsService;
    @Getter
    @Setter
    private Products product;
    @Getter
    @Setter
    private Salespoints salespoint;
    @Getter
    @Setter
    private OrderDetails details;



    public void search() {

    }

    public void reset() {
        salespoint = null;
        product = null;
    }

    @Override
    protected List<FilterExample> getFilters() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        List<FilterExample> filters = super.getFilters();
//        filters.add(new FilterExample("createdAt",cal.getTime(), InequalityConstants.GREATER_EQUAL));
        return filters;
    }

    public void init(){
        list=service.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"orderId");
//        orders=new OrderModel(service);
//        orders.setFilters(getFilters());
//        orders.setProperty("createdAt");
//        orders.setSortEnum(SortEnum.DESCENDING);
    }

    public void delete(Orders delete) {
        service.remove(delete);
    }

    @Override
    public String edit(Orders edit) {
        setEntity(edit);
        return super.edit(edit);
    }

    public String edit(Integer orderId,OrderDetails details) {
        list=service.findByProperty("orderId",orderId);
        this.details=details;
        return navigate("form.xhtml");
    }


    @Override
    public String add(){
        setEntity(new Orders());
        return super.add();
    }

    public String save(){
        return navigate("list.xhtml");
    }

    public List<Salespoints> getSalespoints(){
        return salespointsService.findAll();
    }
    public List<Products> getProducts(){
        return productsService.findAll();
    }
}
