package kg.dutyfree.adminsystem.controller.admin;

import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Country;
import kg.dutyfree.adminsystem.entity.Tradepoints;
import kg.dutyfree.adminsystem.enums.ShopType;
import kg.dutyfree.adminsystem.service.CountryService;
import kg.dutyfree.adminsystem.service.TradepointsService;
import lombok.Getter;
import lombok.Setter;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@SessionScoped
public class TradepointBean extends BaseBean<Tradepoints> {
    @EJB
    private TradepointsService service;
    @EJB
    private CountryService countryService;
    @Setter
    @Getter
    private List<ShopType> types;

    @Override
    public String add() {
        setEntity(new Tradepoints());
        return super.add();
    }

    public void init(){
//        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"basic.addDate");
        types=new ArrayList<>();
        types.add(ShopType.AEROPORT);
        types.add(ShopType.BORDERSHOP);
        list= service.findAll();
    }

    @Override
    public String edit(Tradepoints edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(Tradepoints delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }

    public List<Country> getCountries(){
        return countryService.findAll();
    }
}
