package kg.dutyfree.adminsystem.controller.admin;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Brands;
import kg.dutyfree.adminsystem.entity.Category;
import kg.dutyfree.adminsystem.entity.Products;
import kg.dutyfree.adminsystem.entity.Subcategories;
import kg.dutyfree.adminsystem.model.ParamModel;
import kg.dutyfree.adminsystem.service.BrandsService;
import kg.dutyfree.adminsystem.service.CategoryService;
import kg.dutyfree.adminsystem.service.ProductsService;
import kg.dutyfree.adminsystem.service.SubcategoriesService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.UploadedFile;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

@Named
@SessionScoped
public class ProductBean extends BaseBean<Products> {
    @EJB
    private ProductsService service;
    @EJB
    private BrandsService brandsService;
    @EJB
    private CategoryService categoryService;
    @EJB
    private SubcategoriesService subcategoriesService;

    @Getter
    @Setter
    private String key;
    @Getter
    @Setter
    private String value;
    @Setter
    @Getter
    private List<ParamModel> paramList;


    @Override
    public String add() {
        setEntity(new Products());
        paramList=new ArrayList<>();
        return super.add();
    }

    public String upload(Products edit) {
        entity = edit;
        return navigate("upload.xhtml");
    }

    @Setter
    @Getter
    private UploadedFile file;

    public String uploadFile() {
        String filePath = this.uploadFile("product/", file);
        if (filePath != null) {
            entity.setImage(filePath);
            service.merge(entity);
            System.out.println("Successful" + file.getFileName() + " is uploaded.");
            entity = null;
            return navigate("list.xhtml");
        }
        return null;
    }

    public void init() {
//        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"basic.addDate");
        list = service.findAll();
    }

    @Override
    public String edit(Products edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);

        return super.edit(edit);
    }

    public void delete(Products delete) {
        service.remove(delete);
    }

    public String save() {
        if (entity.getId() == null) {
            ParamModel first=new ParamModel("Бренд",entity.getBrands().getName());
            paramList.add(first);
            entity.setParams(new Gson().toJson(paramList));
            entity = service.persist(entity);
        } else {
//            entity.getBasic().setChangeDate(new Date());
            entity = service.merge(entity);
        }
        return navigate("list.xhtml");
    }

    public List<Brands> getBrands() {
        return brandsService.findAll();
    }

    public List<Category> getCategories() {
        return categoryService.findAll();
    }

    public List<Subcategories> getSubcategories() {
        if (entity.getCategory() == null)
            return subcategoriesService.findAll();
        return subcategoriesService.findByProperty("category", entity.getCategory());
    }

    public String parametres(Products products){
        Type empMapType = new TypeToken<List<ParamModel>>() {}.getType();
        entity=products;
        paramList = new Gson().fromJson(entity.getParams(), empMapType);
        System.out.println("edit:"+paramList);
        return navigate("parameters.xhtml");
    }

    public void addParam(){
        paramList.add(new ParamModel(key,value));
        entity.setParams(new Gson().toJson(paramList));
        service.merge(entity);
    }

    public void removeParam(ParamModel model){
        System.out.println(model);
        paramList.remove(model);
        entity.setParams(new Gson().toJson(paramList));
        service.merge(entity);
    }
    public void removeParam(){
        System.out.println("model");
    }

}
