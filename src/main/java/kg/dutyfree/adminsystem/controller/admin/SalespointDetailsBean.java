package kg.dutyfree.adminsystem.controller.admin;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.SalespointDetails;
import kg.dutyfree.adminsystem.entity.SalespointDetails;
import kg.dutyfree.adminsystem.entity.Salespoints;
import kg.dutyfree.adminsystem.entity.Tradepoints;
import kg.dutyfree.adminsystem.service.SalespointDetailsService;
import kg.dutyfree.adminsystem.service.SalespointsService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.List;

@Named
@SessionScoped
public class SalespointDetailsBean extends BaseBean<SalespointDetails> {
    @EJB
    private SalespointDetailsService service;
    @EJB
    private SalespointsService salespointsService;

    @Setter
    @Getter
    private Double lat;
    @Setter
    @Getter
    private Double lon;
    @Setter
    @Getter
    private Salespoints salespoints;
    @Setter
    @Getter
    private MapModel emptyModel;

    public void addMarker() {
        Marker marker = new Marker(new LatLng(lat, lon), salespoints.getName());
        emptyModel.addOverlay(marker);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Added", "Lat:" +lat + ", Lng:" + lon));
    }



    @Override
    public String add() {
        setEntity(new SalespointDetails());
        return super.add();
    }

    public String details(Salespoints salespoints){
        setEntity(new SalespointDetails());
        this.salespoints=salespoints;
        emptyModel = new DefaultMapModel();
        return navigate("details.xhtml");
    }

    public void init(){
//        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"basic.addDate");
        list= service.findAll();
    }

    @Override
    public String edit(SalespointDetails edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(SalespointDetails delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity.setSalespoint(salespoints);
            JsonObject json= new JsonObject();
            json.addProperty("lat",lat);
            json.addProperty("lon",lon);
            entity.setCoordinates(json.toString());
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }

    public List<Salespoints> getSalespoints(){
        return salespointsService.findAll();
    }
}
