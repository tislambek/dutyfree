package kg.dutyfree.adminsystem.controller.admin;

import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Category;
import kg.dutyfree.adminsystem.entity.SalespointCurrencies;
import kg.dutyfree.adminsystem.entity.Subcategories;
import kg.dutyfree.adminsystem.service.CategoryService;
import kg.dutyfree.adminsystem.service.SubcategoriesService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.UploadedFile;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.List;

@Named
@SessionScoped
public class SubCategoryBean extends BaseBean<Subcategories> {
    @EJB
    private SubcategoriesService service;
    @EJB
    private CategoryService categoryService;

    @Override
    public String add() {
        setEntity(new Subcategories());
        return super.add();
    }

    public void init(){
//        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"basic.addDate");
        list= service.findAll();
    }

    @Override
    public String edit(Subcategories edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(Subcategories delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }

    public List<Category> getCategories(){
        return categoryService.findAll();
    }
}
