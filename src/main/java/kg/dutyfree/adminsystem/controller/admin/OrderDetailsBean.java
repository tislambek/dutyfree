package kg.dutyfree.adminsystem.controller.admin;


import kg.dutyfree.adminsystem.beans.FilterExample;
import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.*;
import kg.dutyfree.adminsystem.enums.InequalityConstants;
import kg.dutyfree.adminsystem.enums.OrdersEnum;
import kg.dutyfree.adminsystem.enums.SortEnum;
import kg.dutyfree.adminsystem.model.OrderModel;
import kg.dutyfree.adminsystem.service.*;
import lombok.Getter;
import lombok.Setter;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author aziko
 */

@Named
@SessionScoped
public class OrderDetailsBean extends BaseBean<OrderDetails> {
    @EJB
    private OrderDetailsService service;
    @EJB
    private OrdersService ordersService;
    @Getter
    @Setter
    private Products product;
    @Getter
    @Setter
    private Salespoints salespoint;
    @Getter
    @Setter
    private OrderModel orders;
    @Setter
    @Getter
    private List<Orders> ordersList;




    public void search() {

    }

    @Override
    protected List<FilterExample> getFilters() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        List<FilterExample> filters = super.getFilters();
        filters.add(new FilterExample("createdAt",cal.getTime(), InequalityConstants.GREATER_EQUAL));
        filters.add(new FilterExample("status", Arrays.asList(OrdersEnum.CREATED,OrdersEnum.ORDERED,OrdersEnum.EXPIRED), InequalityConstants.IN));
        return filters;
    }

    public void init(){
        list=service.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"orderId");
//        orders=new OrderModel(service);
//        orders.setFilters(getFilters());
//        orders.setProperty("createdAt");
//        orders.setSortEnum(SortEnum.DESCENDING);
    }

    public void delete(OrderDetails delete) {
        service.remove(delete);
    }

    @Override
    public String edit(OrderDetails edit) {
        setEntity(edit);
        ordersList=ordersService.findByProperty("orderId",edit.getOrderId());
        return super.edit(edit);
    }




    @Override
    public String add(){
        setEntity(new OrderDetails());
        return super.add();
    }

    public String save(){
        if (entity.getId()==null){
            service.persist(entity);
        }else{
            OrdersEnum status=null;
            if (entity.getStatus().equals(OrdersEnum.CREATED)){
                   status=OrdersEnum.ORDERED;
            }else if(entity.getStatus().equals(OrdersEnum.ORDERED)){
                status=OrdersEnum.CONFIRMED;
            }
            if (status!=null) {
                entity.setStatus(status);
            }
            service.merge(entity);
        }
        return navigate("list.xhtml");
    }

}
