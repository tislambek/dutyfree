package kg.dutyfree.adminsystem.controller.admin;


import kg.dutyfree.adminsystem.beans.FilterExample;
import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Role;
import kg.dutyfree.adminsystem.entity.User;
import kg.dutyfree.adminsystem.enums.InequalityConstants;
import kg.dutyfree.adminsystem.enums.SortEnum;
import kg.dutyfree.adminsystem.service.RoleService;
import kg.dutyfree.adminsystem.service.UserService;
import lombok.Getter;
import lombok.Setter;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aziko
 */

@Named
@SessionScoped
public class UserBean extends BaseBean<User> {
    @EJB
    private UserService userService;
    @EJB
    private RoleService roleService;

    @Getter
    @Setter
    private String password;

//    List<FilterExample> getFilters(){
//        List<FilterExample> result=new ArrayList<>();
//        return result;
//    }


    @Override
    protected List<FilterExample> getFilters() {
        List<FilterExample> filters = super.getFilters();
        filters.add(new FilterExample("username",loginUtil.getCurrentUser().getUsername(), InequalityConstants.NOT_EQUAL));
        return filters;
    }

    public void init(){
        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"createdAt");
//        list= userService.findAll();
    }

    public void delete(User delete) {

    }

    @Override
    public String edit(User edit) {
        setEntity(edit);
        return super.edit(edit);
    }


    @Override
    public String add(){
        setEntity(new User());
        return super.add();
    }

    public String save(){
        if (entity.getId()==null){
            userService.persist(entity);
        }else{
            userService.merge(entity);
        }
        return navigate("list.xhtml");
    }

    public List<Role> getRoles(){
        return roleService.findAll();
    }
}
