package kg.dutyfree.adminsystem.controller.admin;

import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Category;
import kg.dutyfree.adminsystem.service.CategoryService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.UploadedFile;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.util.Date;

@Named
@SessionScoped
public class CategoryBean extends BaseBean<Category> {
    @EJB
    private CategoryService service;
    @Override
    public String add() {
        setEntity(new Category());
        return super.add();
    }

    public String upload(Category edit) {
        entity = edit;
        return navigate("upload.xhtml");
    }

    @Setter
    @Getter
    private UploadedFile file;

    public String uploadFile() {
        String filePath = this.uploadFile("category/", file);
        if (filePath!=null){
            entity.setImage(filePath);
            service.merge(entity);
            System.out.println("Successful" + file.getFileName() + " is uploaded.");
            entity=null;
            return navigate("list.xhtml");
        }
        return null;
    }

    public void init(){
//        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"basic.addDate");
        list= service.findAll();
    }

    @Override
    public String edit(Category edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(Category delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }
}
