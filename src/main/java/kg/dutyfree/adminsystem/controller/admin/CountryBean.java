package kg.dutyfree.adminsystem.controller.admin;

import kg.dutyfree.adminsystem.controller.BaseBean;
import kg.dutyfree.adminsystem.entity.Country;
import kg.dutyfree.adminsystem.service.CountryService;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.UploadedFile;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class CountryBean extends BaseBean<Country> {
    @EJB
    private CountryService service;
    @Override
    public String add() {
        setEntity(new Country());
        return super.add();
    }

    public String upload(Country edit) {
        entity = edit;
        return navigate("upload.xhtml");
    }

    @Setter
    @Getter
    private UploadedFile file;

    public String uploadFile() {
        String filePath = this.uploadFile("Country/", file);
        if (filePath!=null){
//            entity.setImage(filePath);
            service.merge(entity);
            System.out.println("Successful" + file.getFileName() + " is uploaded.");
            entity=null;
            return navigate("list.xhtml");
        }
        return null;
    }

    public void init(){
//        list=userService.findByExample(0,0, SortEnum.DESCENDING,getFilters(),"basic.addDate");
        list= service.findAll();
    }

    @Override
    public String edit(Country edit) {
        System.out.println("teste tsetsetkjsdkl ");
        setEntity(edit);
        return super.edit(edit);
    }

    public void delete(Country delete) {
        service.remove(delete);
    }

    public String save(){
        if(entity.getId()==null){
            entity=service.persist(entity);
        }else{
            entity=service.merge(entity);
        }
        return navigate("list.xhtml");
    }
}
