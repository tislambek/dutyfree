package kg.dutyfree.adminsystem.controller;

import kg.dutyfree.adminsystem.entity.Role;
import kg.dutyfree.adminsystem.entity.User;
import kg.dutyfree.adminsystem.entity.basic.Basic;
import kg.dutyfree.adminsystem.service.RoleService;
import kg.dutyfree.adminsystem.service.UserService;
import kg.dutyfree.adminsystem.util.*;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by kbakytbekov on 08.09.2017.
 */
@Named
@SessionScoped
public class AuthBean implements Serializable {
    @EJB
    private UserService userService;
    @EJB
    private RoleService roleService;

    @Inject
    private LoginUtil loginUtil;
    @Inject
    private Resources resources;

    private String username;
    private String password;


    public String login() throws IOException {
        User user = userService.getByProperty("username", username);

        if (user != null && loginUtil.isPasswordsMatch(password, user.getPassword(), user.getSalt())) {
            return authorize(user);
        } else {
            Messages.showMessage("form", "invalidUsernameOrPassword", FacesMessage.SEVERITY_ERROR, false);
            return null;
        }

    }

    public String authorize(User user) {
        if (loginUtil.userHasRole(user, "admin")) {
            loginUtil.setCurrentUser(user);
            return navigate("/admin/category/list.xhtml");
        } else if (loginUtil.userHasRole(user, "moderator")) {
            loginUtil.setCurrentUser(user);
            return navigate("/moderator/orders/list.xhtml");
        } else if (loginUtil.userHasRole(user, "superadmin")) {
            loginUtil.setCurrentUser(user);
            return navigate("/superadmin/orders/list.xhtml");
        }  else {
            return null;
        }
    }

    public void add() {
        System.out.println("-------------------------------------------");
        Role role = new Role();
        role.setDescription("Administrator");
        role.setTitle("admin");
        roleService.persist(role);
        Role role1 = new Role();
        role1.setDescription("Moderator");
        role1.setTitle("moderator");
        roleService.persist(role1);
        Role role2 = new Role();
        role2.setDescription("User");
        role2.setTitle("user");
        roleService.persist(role2);
        User user = new User();
//        user.setEmail("test");
        user.setSalt(PasswordEncryptionService.generateSalt());
        user.setPassword(PasswordEncryptionService.hashPassword("test", user.getSalt()));
        user.setUsername("test");
        user.setCreatedAt(LocalDateTime.now());

        user.setRole(roleService.findById(Long.valueOf(1), false));
        userService.persist(user);
    }

    public void logout() {
        loginUtil.logout();
        Util.redirect("/index.xhtml");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public boolean isAdmin() {
        boolean admin = loginUtil.getCurrentUser().getRole().getTitle().equals("admin");
        return admin;
    }

    public boolean isOperator() {
        boolean user = loginUtil.getCurrentUser().getRole().getTitle().equals("operator");
        return user;
    }

    public boolean isManager() {
        boolean user = loginUtil.getCurrentUser().getRole().getTitle().equals("manager");
        return user;
    }

    public boolean isSuperadmin() {
        boolean user = loginUtil.getCurrentUser().getRole().getTitle().equals("superadmin");
        return user;
    }

    public User checkUser() {
        return loginUtil.getCurrentUser();
    }

    public void redirectToIndex() {
        Util.redirect("/index.xhtml");
    }

    private String navigate(String url) {
        return url + "?faces-redirect=true";
    }
}
