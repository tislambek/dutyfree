package kg.dutyfree.adminsystem.controller;

import kg.dutyfree.adminsystem.beans.FilterExample;
import kg.dutyfree.adminsystem.entity.basic.AbstractEntity;
import kg.dutyfree.adminsystem.entity.basic.Basic;
import kg.dutyfree.adminsystem.util.LoginUtil;
import kg.dutyfree.adminsystem.util.Util;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.UploadedFile;

import javax.inject.Inject;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by kbakytbekov on 09.09.2017.
 */
public class BaseBean<T extends AbstractEntity> implements Serializable {
    @Inject
    protected LoginUtil loginUtil;

    @Getter
    @Setter
    protected List<T> list;

    @Setter
    @Getter
    protected T entity;

    protected String uploadFile(String directory, UploadedFile file) {
        if (file != null) {
            String filePath = directory + UUID.randomUUID().toString()+ "_"+file.getFileName();
            try {
                copyFile(Util.baseUrl,filePath, file.getInputstream());
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return filePath;
        }
        return null;
    }

    private void copyFile(String uploadPath, String fileName, InputStream in) {
        try {

            // write the inputStream to a FileOutputStream
            OutputStream out = new FileOutputStream(new File(uploadPath + fileName));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

            System.out.println("New file created!");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    protected Basic basic=new Basic();

    protected String navigate(String url) {
        return url + "?faces-redirect=true";
    }

    public String getFormatString(Date date){
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    protected void writeLog(String action) {
//        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        String ipAddress = request.getHeader("X-FORWARDED-FOR");
//        if (ipAddress == null) {
//            ipAddress = request.getRemoteAddr();
//        }
//        System.out.println("ipAddress:" + ipAddress);
//        Log log=new Log();
//        log.setAction(action);
//        log.setIp(ipAddress);
//        log.setLogDate(new Date());
//        log.setUsername(loginUtil.getCurrentUser().getUsername());
//        logService.persist(log);
    }

    public String add(){
        setEntity(entity);
        return navigate("form.xhtml");
    }

    public String edit(T edit){
        entity=edit;
        return navigate("form.xhtml");
    }

    public String back(){
        entity=null;
        return navigate("list.xhtml");
    }

    protected List<FilterExample> getFilters(){
        return new ArrayList<>();
    }
}
