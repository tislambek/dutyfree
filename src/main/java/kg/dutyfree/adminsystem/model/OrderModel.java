package kg.dutyfree.adminsystem.model;

import kg.dutyfree.adminsystem.dao.OrdersDao;
import kg.dutyfree.adminsystem.entity.Orders;
import kg.dutyfree.adminsystem.service.OrdersService;
import kg.dutyfree.adminsystem.util.Converter;

import java.math.BigInteger;

public class OrderModel extends BaseModel<Orders, Long, OrdersDao, OrdersService> {
    public OrderModel(OrdersService service) {
        super(service);
    }

    @Override
    protected Long getKey(String rowKey) {
        return Converter.strToLong(rowKey);
    }
}
