package kg.dutyfree.adminsystem.model;


import kg.dutyfree.adminsystem.beans.FilterExample;
import kg.dutyfree.adminsystem.entity.basic.PersistentEntity;
import kg.dutyfree.adminsystem.service.GenericService;
import kg.dutyfree.adminsystem.dao.GenericDao;
import kg.dutyfree.adminsystem.enums.SortEnum;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author aziko
 */

public abstract class BaseModel<T extends PersistentEntity<ID>, ID extends Serializable, D extends GenericDao<T, ID>,
        S extends GenericService<T, ID>> extends LazyDataModel<T> {

    protected List<FilterExample> filters;
    protected String[] fields;
    protected String property = "id";
    protected SortEnum sortEnum = SortEnum.ASCENDING;
    private int rowCount;
    private S service;
    private int first;

    public BaseModel(S service) {
        this.service = service;
        initRowCount();
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public Object getRowKey(T object) {
        return object.getId();
    }

    @Override
    public T getRowData(String rowKey) {
        return service.findById(getKey(rowKey), false);
    }

    public List<FilterExample> getFilters() {
        return filters;
    }

    public void setFilters(List<FilterExample> filters) {
        this.filters = filters;
        initRowCount();
    }

    public String[] getFields() {
        return fields;
    }

    public void setFields(String[] fields) {
        this.fields = fields;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public SortEnum getSortEnum() {
        return sortEnum;
    }

    public void setSortEnum(SortEnum sortEnum) {
        this.sortEnum = sortEnum;
    }

    @Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        try {
            if (this.filters == null) this.filters = new ArrayList<>();

            List<T> entities = fields == null ? service.findByExample(first, pageSize, getSortEnum(), this.filters, getProperty())
                    : service.findByExample(first, pageSize, getSortEnum(), this.filters, getSortProperty(), fields);

            return entities;
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    protected String getSortProperty() {
        return "id";
    }

    protected abstract ID getKey(String rowKey);

    private void initRowCount() {
        try {
            if (this.filters != null) {
                rowCount = service.countByExample(filters).intValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            rowCount = 0;
        }
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }
}
