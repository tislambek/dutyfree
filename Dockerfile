FROM jboss/wildfly:9.0.1.Final

WORKDIR /opt/jboss
COPY modules/ /opt/jboss/wildfly/modules/
COPY postgresql-42.6.0.jar /opt/jboss/wildfly/modules/system/layers/base/org/postgresql/main/
COPY standalone.xml /opt/jboss/wildfly/standalone/configuration/standalone.xml
COPY target/adminsystem.war /opt/jboss/wildfly/standalone/deployments/
EXPOSE 8080 9990
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0"]
